# __author__ = 'Pawel Polit'

import socket
import httplib
from urlparse import urlparse
import ast
from threading import Thread
import time
import os

import scan

HOST = 'localhost'
PORT = 4567

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

clients_url = urlparse('http://' + HOST + ':' + str(PORT) + '/CLIENTS/')

last_modification_date = scan.scan('sync')[2]
received_messages = None
sender = None
download_completed = False


class StayConnected(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        while True:
            sock.sendto('Pawel', (HOST, PORT))

            connection = httplib.HTTPConnection(clients_url.netloc)
            connection.request('GET', clients_url.path)
            response = connection.getresponse()
            clients = response.read()
            clients = ast.literal_eval(clients)
            clients = [client[1] for client in clients]

            for client in clients:
                client_host, client_port = client.split(':')
                sock.sendto('Pawel', (client_host, int(client_port)))

            time.sleep(10)


class CheckFolderState(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        global last_modification_date, received_messages, download_completed, sender

        while True:
            local_state, local_chunks, local_modification_date = scan.scan('sync')

            connection = httplib.HTTPConnection(HOST + ":" + str(PORT))
            connection.request("GET", "/")
            response = connection.getresponse()
            server_state = ast.literal_eval(response.read())

            if local_state != server_state:
                if local_modification_date == last_modification_date:
                    local_files_names = [name_chunk[0] for name_chunk in local_state]
                    server_files_names = [name_chunk[0] for name_chunk in server_state]

                    for local_file_name in local_files_names:
                        if local_file_name not in server_files_names:
                            os.remove("sync/" + local_file_name)

                    files_to_download = [server_file for server_file in server_state if server_file not in local_state]

                    message = '["checkState", ' + str(server_state) + ']'

                    connection = httplib.HTTPConnection(clients_url.netloc)
                    connection.request('GET', clients_url.path)
                    response = connection.getresponse()
                    clients = response.read()
                    clients = ast.literal_eval(clients)
                    clients = [client[1] for client in clients]

                    received_messages = 0

                    for client in clients:
                        client_host, client_port = client.split(':')
                        sock.sendto(message, (client_host, int(client_port)))

                    while received_messages < len(clients):
                        pass

                    download_completed = False
                    sock.sendto('["give_me_file", ' + str(files_to_download) + ']', sender)

                    while not download_completed:
                        pass

                else:
                    connection = httplib.HTTPConnection(clients_url.netloc)
                    connection.request("POST", "/", str(local_state))
                    connection.getresponse()
                    last_modification_date = local_modification_date

            time.sleep(10)


def start_reading():
    global received_messages, sender, download_completed

    opened_files = {}

    while True:
        message, address = sock.recvfrom(4096)
        print address, message

        if message[0] == '[' and message[-1] == ']':

            message = ast.literal_eval(message)

            if message[0] == 'checkState':
                if scan.scan('sync')[0] == message[1]:
                    sock.sendto('["has_file", True]', address)
                else:
                    sock.sendto('["has_file", False]', address)

            elif message[0] == 'has_file':
                received_messages += 1

                if message[1]:
                    sender = address

            elif message[0] == 'give_me_file':
                files_to_send = message[1]

                for name, chunk in files_to_send:
                    sock.sendto('["next_file", "sync/' + name + '"]', address)

                    with open('sync/' + name, 'r') as file_to_send:
                        send_buffer = file_to_send.read(2048)

                        while send_buffer:
                            sock.sendto('["file_part", "sync/' + name + '", """' + str(send_buffer) + '"""]',
                                        address)
                            send_buffer = file_to_send.read(2048)

                        sock.sendto('["EOF", "sync/' + name + '"]', address)

                sock.sendto('["end"]', address)

            elif message[0] == 'next_file':
                path_table = message[1].split('/')
                current_path = ''

                for name in path_table[:-1]:
                    current_path += name

                    if not os.path.exists(current_path):
                        os.mkdir(current_path)

                    current_path += '/'

                opened_files[message[1]] = open(message[1], "w")

            elif message[0] == 'EOF':
                opened_files[message[1]].close()

            elif message[0] == 'file_part':
                opened_files[message[1]].write(message[2])

            elif message[0] == 'end':
                download_completed = True
                opened_files = {}


StayConnected().start()
CheckFolderState().start()
start_reading()

#!/usr/bin/env python

from datetime import datetime, timedelta
from threading import Thread
import json
import re
import time
import logging
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import UDPServer, ThreadingMixIn, BaseRequestHandler

HOST_NAME = ''
PORT_NUMBER = 4567

state = '[]'
clients = dict()


class UDPHandler(BaseRequestHandler):
    def handle(self):
        data = self.request[0]
        socket = self.request[1]
        client = ':'.join([str(s) for s in self.client_address])
        ts = datetime.now()
        clients[data] = (client, ts)
        logging.info('Client %s : %s' % (data, client))
        socket.sendto('OK', self.client_address)


class ThreadedUDPServer(ThreadingMixIn, UDPServer):
    pass


class HTTPHandler(BaseHTTPRequestHandler):
    protocol_version = 'HTTP/1.1'

    def respond(self, response):
        self.send_response(200)
        self.send_header("Content-type", "text; charset=utf-8")
        self.send_header("Content-length", str(len(response)))
        self.end_headers()
        self.wfile.write(response)

    def do_GET(self):
        global state, clients
        if self.path == '/':
            self.respond(state)
        elif self.path.startswith('/CHUNK/'):
            chunk = re.sub(r'[^a-f0-9]', '', self.path.split('/')[2])
            if not chunk:
                self.send_response(400)
                self.send_header("Content-length", "0")
                self.end_headers()
            with open('chunk_' + chunk, 'r') as fc:
                data = fc.read()
                self.send_response(200)
                self.send_header("Content-length", str(len(data)))
                self.end_headers()
                self.wfile.write(data)
        elif self.path.startswith('/CLIENTS/'):
            now = datetime.now()
            clients = dict([(k, v) for k, v in clients.items() if now - v[-1] < timedelta(minutes=1)])
            self.respond(json.dumps([(k, v[0]) for k, v in clients.items()]))
        else:
            self.send_response(400)
            self.send_header("Content-length", "0")
            self.end_headers()

    def do_POST(self):
        global state
        length = int(self.headers.getheader('Content-length'))
        if self.path == '/':
            state = self.rfile.read(length)
            self.respond('OK')
        elif self.path.startswith('/CHUNK/'):
            chunk = re.sub(r'[^a-f0-9]', '', self.path.split('/')[2])
            if not chunk:
                self.send_response(400)
                self.end_headers()
            with open('chunk_' + chunk, 'wb') as fc:
                data = self.rfile.read(length)
                fc.write(data)
            self.respond('OK')


class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    pass


logging.basicConfig(level=logging.DEBUG)

httpd = ThreadedHTTPServer((HOST_NAME, PORT_NUMBER), HTTPHandler)
httpt = Thread(target=httpd.serve_forever)
httpt.daemon = True
httpt.start()

udpd = ThreadedUDPServer((HOST_NAME, PORT_NUMBER), UDPHandler)
udpt = Thread(target=udpd.serve_forever)
udpt.daemon = True
udpt.start()

while True:
    time.sleep(1)
